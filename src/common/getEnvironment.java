package common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.firefox.FirefoxDriver;

public class getEnvironment {
	public static void openFirefox() {
		System.getProperty("webdriver.gecko.driver", utils.firefoxDriver);
		utils.driver = new FirefoxDriver();
		//maximize browser
		//utils.driver.manage().window().maximize();
		//implicitly wait
		utils.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
}
