package common;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.junit.Assert;

import pageObject.homeObject;
import pageObject.logInObject;
import pageObject.reportIssureObject;
import pageObject.viewIssuesObject;

public class commonFuntions {
	// Declare: read from excel
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetLogIn = "logIn";
	public static String SheetReportIssue = "reportIssue";

	// log in
	public static void commonLogIn() throws Exception {
		XSSFSheet ExcelDataLogIn = ExcelCommon_POI.setExcelFile(ExcelTestData, SheetLogIn);
		String Username = ExcelCommon_POI.getCellData(1, 1, ExcelDataLogIn);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataLogIn);

		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.txtUsername).sendKeys(Username);
		utils.driver.findElement(logInObject.txtPassword).sendKeys(Password);
		utils.driver.findElement(logInObject.btnLogin).click();

		Thread.sleep(5000);
		Assert.assertEquals(true, utils.driver.getPageSource().contains("Logged in"));
	}

	// Log out
	public static void commonLogOut() throws InterruptedException {
		utils.driver.findElement(homeObject.mnuLogOut).click();
		Thread.sleep(3000);
	}

	// Create Issue
	public static void commonReportIssue() throws Exception {
		// load report issue page
		utils.driver.findElement(homeObject.mnuReportIssue).click();
		Thread.sleep(3000);

		// Read from excel
		XSSFSheet ExcelDataReportIssue = ExcelCommon_POI.setExcelFile(ExcelTestData, SheetReportIssue);
		String category = ExcelCommon_POI.getCellData(1, 1, ExcelDataReportIssue);
		String reproducibility = ExcelCommon_POI.getCellData(1, 2, ExcelDataReportIssue);
		String severity = ExcelCommon_POI.getCellData(1, 3, ExcelDataReportIssue);
		String priority = ExcelCommon_POI.getCellData(1, 4, ExcelDataReportIssue);
		String platform = ExcelCommon_POI.getCellData(1, 5, ExcelDataReportIssue);
		String os = ExcelCommon_POI.getCellData(1, 6, ExcelDataReportIssue);
		String osVersion = ExcelCommon_POI.getCellData(1, 7, ExcelDataReportIssue);
		String assignee = ExcelCommon_POI.getCellData(1, 8, ExcelDataReportIssue);
		String summary = ExcelCommon_POI.getCellData(1, 9, ExcelDataReportIssue);
		String description = ExcelCommon_POI.getCellData(1, 10, ExcelDataReportIssue);
		String reproduce = ExcelCommon_POI.getCellData(1, 11, ExcelDataReportIssue);
		String addInfo = ExcelCommon_POI.getCellData(1, 12, ExcelDataReportIssue);

		// input info
		utils.driver.findElement(reportIssureObject.slbCategory).sendKeys(category);
		utils.driver.findElement(reportIssureObject.slbReproducibility).sendKeys(reproducibility);
		utils.driver.findElement(reportIssureObject.slbSeverity).sendKeys(severity);
		utils.driver.findElement(reportIssureObject.slbPriority).sendKeys(priority);
		utils.driver.findElement(reportIssureObject.txtPlatform).sendKeys(platform);
		utils.driver.findElement(reportIssureObject.txtOS).sendKeys(os);
		utils.driver.findElement(reportIssureObject.txtOSVersion).sendKeys(osVersion);
		utils.driver.findElement(reportIssureObject.slbAssignee).sendKeys(assignee);
		utils.driver.findElement(reportIssureObject.txtSummary).sendKeys(summary);
		utils.driver.findElement(reportIssureObject.txtDescription).sendKeys(description);
		utils.driver.findElement(reportIssureObject.txtReproduce).sendKeys(reproduce);
		utils.driver.findElement(reportIssureObject.txtAdditionalInfo).sendKeys(addInfo);
		utils.driver.findElement(reportIssureObject.btnSubmitReport).click();
		Thread.sleep(5000);
	}

	// Search Issue
	public static void commonSeachIssue() throws InterruptedException {
		// load report issue page
		//utils.driver.findElement(homeObject.mnuReportIssue).click();
		//Thread.sleep(3000);
		// search issue
		utils.driver.findElement(viewIssuesObject.txtSearch).clear();
		utils.driver.findElement(viewIssuesObject.txtSearch).sendKeys("0948251919");
		utils.driver.findElement(viewIssuesObject.btnFilter).click();
		Thread.sleep(3000);
	}
	
	public static void commonDeleteIssue() throws InterruptedException {
		utils.driver.findElement(viewIssuesObject.chkAllBug).click();
		utils.driver.findElement(viewIssuesObject.slbAction).sendKeys("Delete");
		Thread.sleep(3000);
		utils.driver.findElement(viewIssuesObject.btnOK).click();
		Thread.sleep(3000);
		utils.driver.findElement(viewIssuesObject.btnDelete).click();
		Thread.sleep(3000);

	}
}
