package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.getEnvironment;
import common.utils;
import pageObject.logInObject;
import pageObject.signUpObject;

public class checkSignUpNewAccount {
	public String ExcelTestData = "testDate.xlsx";
	public String SheetMaster = "Master";
	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
	}

	@Test
	public void singUp() throws InterruptedException {
		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.lSingUpNewAccount).click();
		Thread.sleep(3000);
		utils.driver.findElement(signUpObject.txtUserName).sendKeys("liennt");
		utils.driver.findElement(signUpObject.txtEmail).sendKeys("liennt@gmail.com");
		utils.driver.findElement(signUpObject.btnSignUp).click();
		if(utils.driver.getPageSource().contains("Congratulations. You have registered successfully." 
		+ "You are now being sent a confirmation e-mail to verify your e-mail address." 
		+ "Visiting the link sent to you in this e-mail will activate your account. "
		+ "You will have seven days to complete the account confirmation process; "
		+ "if you fail to complete account confirmation within seven days, this newly-registered account may be purged. ")){
			ExcelCommon_POI.setCellData(2, 1, ExcelTestData, SheetMaster, "Pass");
		}else{
			ExcelCommon_POI.setCellData(2, 1, ExcelTestData, SheetMaster, "Fail");
		}
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
