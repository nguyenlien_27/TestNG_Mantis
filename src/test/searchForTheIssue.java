package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;
import pageObject.viewIssuesObject;

public class searchForTheIssue {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";
	
/*
 	 @Parameters("browser")
	 @BeforeTest public void before(String browser) throws Exception {
	 OpenMultiBrowser.multi_browser(browser); }
 */
	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}

	@Test
	public void searchIssue() throws Exception {
		//view issues
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		//search issue
		commonFuntions.commonSeachIssue();
		
		if(!utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			ExcelCommon_POI.setCellData(9, 3, ExcelTestData, SheetMaster, "Pass");
		}else{
			commonFuntions.commonReportIssue();
			utils.driver.findElement(viewIssuesObject.txtSearch).clear();
			utils.driver.findElement(viewIssuesObject.txtSearch).sendKeys("0948251919");
			utils.driver.findElement(viewIssuesObject.btnFilter).click();
			if(!utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")){
				ExcelCommon_POI.setCellData(9, 3, ExcelTestData, SheetMaster, "Pass");
			}else{
				ExcelCommon_POI.setCellData(9, 3, ExcelTestData, SheetMaster, "Fail");
			}
		}
		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() throws InterruptedException {
		utils.driver.close();
	}
}
