package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.viewIssuesObject;

public class checkReportIssue {
	// Declare: read from excel
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetReportIssue = "reportIssue";
	public static String SheetMaster = "Master";

	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}

	@Test
	public static void reportIssue() throws Exception {
		commonFuntions.commonReportIssue();
		
		//verify add success or not if
		utils.driver.findElement(viewIssuesObject.txtSearch).clear();
		utils.driver.findElement(viewIssuesObject.txtSearch).sendKeys("0948251919");
		utils.driver.findElement(viewIssuesObject.btnFilter).click();
		Thread.sleep(5000);

		if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			ExcelCommon_POI.setCellData(11, 3, ExcelTestData, SheetMaster, "Failse");
		} else {
			ExcelCommon_POI.setCellData(11, 3, ExcelTestData, SheetMaster, "Pass");
		}
		
		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}

}
