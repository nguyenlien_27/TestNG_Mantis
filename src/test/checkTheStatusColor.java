package test;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;

public class checkTheStatusColor {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";

	/*
	 @Parameters("browser")
	 @BeforeTest public void before(String browser) throws Exception {
	 OpenMultiBrowser.multi_browser(browser); }
	 */
	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}

	@Test
	public void verifyColor() throws InterruptedException {
		String colorForNewStt = utils.driver.findElement(homeObject.colorSttNew).getAttribute("bgcolor");
		String colorForFeedbackStt = utils.driver.findElement(homeObject.colorSttFeedback).getAttribute("bgcolor");
		String colorForAcknowledgedStt = utils.driver.findElement(homeObject.colorSttAcknowledged)
				.getAttribute("bgcolor");
		String colorForConfirmedStt = utils.driver.findElement(homeObject.colorSttConfirmed).getAttribute("bgcolor");
		String colorForAssignedStt = utils.driver.findElement(homeObject.colorSttAssigned).getAttribute("bgcolor");
		String colorResolvedStt = utils.driver.findElement(homeObject.colorSttResolved).getAttribute("bgcolor");
		String colorColosedStt = utils.driver.findElement(homeObject.colorSttClosed).getAttribute("bgcolor");

		Assert.assertEquals("#fcbdbd", colorForNewStt);
		Assert.assertEquals("#e3b7eb", colorForFeedbackStt);
		Assert.assertEquals("#ffcd85", colorForAcknowledgedStt);
		Assert.assertEquals("#fff494", colorForConfirmedStt);
		Assert.assertEquals("#c2dfff", colorForAssignedStt);
		Assert.assertEquals("#d2f5b0", colorResolvedStt);
		Assert.assertEquals("#c9ccc4", colorColosedStt);
		if (colorForNewStt.equals("#fcbdbd") && colorForFeedbackStt.equals("#e3b7eb")
				&& colorForAcknowledgedStt.equals("#ffcd85") && colorForConfirmedStt.equals("#fff494")
				&& colorForAssignedStt.equals("#c2dfff") && colorResolvedStt.equals("#d2f5b0")
				&& colorColosedStt.equals("#c9ccc4")) {
			ExcelCommon_POI.setCellData(12, 3, ExcelTestData, SheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(12, 3, ExcelTestData, SheetMaster, "Fail");
		}
		
		commonFuntions.commonLogOut();
	}
	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}

