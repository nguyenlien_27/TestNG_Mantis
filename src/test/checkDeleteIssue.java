package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;

public class checkDeleteIssue {
	public String ExcelTestData = "testData.xlsx";
	public String SheetMaster = "Master";

	/*
	 * @Parameters("browser")
	 * 
	 * @BeforeTest public void before(String browser) throws Exception {
	 * OpenMultiBrowser.multi_browser(browser); }
	 */

	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();

	}

	@Test
	public void deleteIssue() throws Exception {
		// view issue
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		// search issue
		commonFuntions.commonSeachIssue();
		// issue is not found
		if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// report issue
			commonFuntions.commonReportIssue();
			// search issue
			commonFuntions.commonSeachIssue();
			// del issue
			commonFuntions.commonDeleteIssue();
			// search again
			commonFuntions.commonSeachIssue();
			if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
				ExcelCommon_POI.setCellData(5, 3, ExcelTestData, SheetMaster, "Pass");
			} else {
				ExcelCommon_POI.setCellData(5, 3, ExcelTestData, SheetMaster, "Fail");
			}
		} else {
			// del issue
			commonFuntions.commonDeleteIssue();
			// search again
			commonFuntions.commonSeachIssue();
			if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
				ExcelCommon_POI.setCellData(5, 3, ExcelTestData, SheetMaster, "Pass");
			} else {
				ExcelCommon_POI.setCellData(5, 3, ExcelTestData, SheetMaster, "Fail");
			}
		}

		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
