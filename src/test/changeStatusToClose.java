package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;
import pageObject.viewIssuesObject;

public class changeStatusToClose {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";
	
	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}

	@Test
	public void changeSttToClose() throws Exception {
		// load view issues pagge
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);
		// search issue
		commonFuntions.commonSeachIssue();
		if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			commonFuntions.commonReportIssue();
			commonFuntions.commonSeachIssue();
			// change stt
			utils.driver.findElement(viewIssuesObject.chkAllBug).click();
			utils.driver.findElement(viewIssuesObject.slbAction).sendKeys("Close");
			utils.driver.findElement(viewIssuesObject.btnOK).click();
			Thread.sleep(3000);
			utils.driver.findElement(viewIssuesObject.txtAddNote).sendKeys("Lien Test");
			utils.driver.findElement(viewIssuesObject.btnCloseIssue).click();
		} else {
			// change stt
			utils.driver.findElement(viewIssuesObject.chkAllBug).click();
			utils.driver.findElement(viewIssuesObject.slbAction).sendKeys("Close");
			utils.driver.findElement(viewIssuesObject.btnOK).click();
			Thread.sleep(3000);
			utils.driver.findElement(viewIssuesObject.txtAddNote).sendKeys("Lien Test");
			utils.driver.findElement(viewIssuesObject.btnCloseIssue).click();
		}
		
		commonFuntions.commonSeachIssue();
		if (utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")){
			ExcelCommon_POI.setCellData(6, 3, ExcelTestData, SheetMaster, "Pass");
		}else{
			ExcelCommon_POI.setCellData(6, 3, ExcelTestData, SheetMaster, "Fail");
		}
		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() {
		utils.driver.quit();
	}
}
