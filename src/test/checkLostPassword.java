package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.getEnvironment;
import common.utils;
import pageObject.logInObject;

public class checkLostPassword {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";

	@BeforeMethod
	public void before() {
		getEnvironment.openFirefox();
	}

	/*
	 @Parameters("browser")
	 @BeforeTest public void before(String browser) throws Exception {
	 OpenMultiBrowser.multi_browser(browser); }
	 */

	@Test
	public void checkLostPass() throws InterruptedException {
		utils.driver.get(utils.URL);
		utils.driver.findElement(logInObject.lLostPass).click();
		Thread.sleep(3000);
		utils.driver.findElement(logInObject.txtUsername).sendKeys("test");
		utils.driver.findElement(logInObject.textEmail).sendKeys("sa@gmail.com");
		utils.driver.findElement(logInObject.btnSubmit).click();
		Thread.sleep(3000);
		if (utils.driver.getPageSource()
				.contains("If you supplied the correct username and e-mail address for"
						+ " your account, we will now have sent a confirmation message to that e-mail address. "
						+ "Once the message has been received, follow the instructions provided to change"
						+ " the password on your account.")) {
			ExcelCommon_POI.setCellData(4, 3, ExcelTestData, SheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(4, 3, ExcelTestData, SheetMaster, "Failse");
		}
		utils.driver.findElement(logInObject.lProceed).click();
		
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
