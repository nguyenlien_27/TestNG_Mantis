package test;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;

public class checkAccessLevel {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";

	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}
/*	
	@Parameters("browser")
	@BeforeTest
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
	}
*/

	@Test
	public void accessLevel() throws InterruptedException {
		utils.driver.findElement(homeObject.mnuMyAccount).click();
		Thread.sleep(5000);
		Assert.assertEquals(true, utils.driver.getPageSource().contains("reporter"));
		Assert.assertEquals(true, utils.driver.getPageSource().contains("developer"));
		if (utils.driver.getPageSource().contains("reporter") && utils.driver.getPageSource().contains("developer")) {
			ExcelCommon_POI.setCellData(1, 3, ExcelTestData, SheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(1, 3, ExcelTestData, SheetMaster, "Failse");
		}
		
		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
