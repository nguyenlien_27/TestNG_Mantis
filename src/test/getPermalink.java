package test;


import org.openqa.selenium.By;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;
import pageObject.viewIssuesObject;

public class getPermalink {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";
	public static String Sheetpermalink = "permalink";

	@BeforeTest
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}

	@Test
	public void creatPermalink() throws InterruptedException {
		// view issues list
		utils.driver.findElement(homeObject.mnuViewIssues).click();
		Thread.sleep(3000);

		// Click on create permalink
		utils.driver.findElement(viewIssuesObject.lCreatePermalink).click();
		Thread.sleep(3000);
		/*
		 * // switch to second windows String winHandle =
		 * utils.driver.getWindowHandle();
		 * utils.driver.switchTo().window(winHandle); Thread.sleep(5000); String
		 * permalink =
		 * utils.driver.findElement(By.xpath("html/body/div[2]/p/a")).getText();
		 * ExcelCommon_POI.setCellData(1, 1, ExcelTestData, Sheetpermalink,
		 * permalink);
		 * 
		 * // switch to parent windows
		 * utils.driver.switchTo().window(winHandle);
		 */
		String parentHandle = utils.driver.getWindowHandle();
		for ( String winHandle : utils.driver.getWindowHandles()) { // Gets the new windows handle
			utils.driver.switchTo().window(winHandle); 
			String permalink = utils.driver.findElement(By.xpath("html/body/div[2]/p/a")).getText();
			System.out.println(permalink);
			Thread.sleep(3000);
		//	ExcelCommon_POI.setCellData(1, 1, ExcelTestData, Sheetpermalink, permalink);
		}
		//utils.driver.close(); // close newly opened window when done with it
		utils.driver.switchTo().window(parentHandle);
	}

	@AfterTest
	public void after() {
		utils.driver.close();
	}
}
