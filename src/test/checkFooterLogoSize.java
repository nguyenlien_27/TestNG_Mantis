package test;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.commonFuntions;
import common.getEnvironment;
import common.utils;
import pageObject.homeObject;

public class checkFooterLogoSize {
	public static String ExcelTestData = "testData.xlsx";
	public static String SheetMaster = "Master";
/*
	@Parameters("browser")
	@BeforeTest
	public void before(String browser) throws Exception {
		OpenMultiBrowser.multi_browser(browser);
	}
*/
	@BeforeMethod
	public void before() throws Exception {
		getEnvironment.openFirefox();
		commonFuntions.commonLogIn();
	}
	
	@Test
	public void verifyLogoSize() throws InterruptedException {
		String logoWidth = utils.driver.findElement(homeObject.imgFooterLogo).getAttribute("width");
		String logoHeight = utils.driver.findElement(homeObject.imgFooterLogo).getAttribute("height");

		Assert.assertEquals("50", logoHeight);
		Assert.assertEquals("145", logoWidth);

		if (logoHeight.equals("50") && logoWidth.equals("145")) {
			ExcelCommon_POI.setCellData(13, 3, ExcelTestData, SheetMaster, "Pass");
		} else {
			ExcelCommon_POI.setCellData(13, 3, ExcelTestData, SheetMaster, "Failse");
		}
		
		commonFuntions.commonLogOut();
	}

	@AfterMethod
	public void after() {
		utils.driver.close();
	}
}
