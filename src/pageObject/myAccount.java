package pageObject;

import org.openqa.selenium.By;

public class myAccount {
	public static By table = By.xpath("html/body/div[2]/form/table");
	public static By lblEditAcc = By.xpath("html/body/div[2]/form/table/tbody/tr[1]/td[1]");
	public static By lblUserName = By.xpath("html/body/div[2]/form/table/tbody/tr[2]/td[1]");
	public static By lblPassword = By.xpath("html/body/div[2]/form/table/tbody/tr[3]/td[1]");
	public static By lblConfirmPassword = By.xpath("html/body/div[2]/form/table/tbody/tr[4]/td[1]");
}
