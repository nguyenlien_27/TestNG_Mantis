package pageObject;

import org.openqa.selenium.By;

public class homeObject {
	//Header items
	
	//Menu items
	public static By mnuViewIssues = By.linkText("View Issues");
	public static By mnuReportIssue = By.linkText("Report Issue");
	public static By mnuLogOut = By.xpath("html/body/table[2]/tbody/tr/td[1]/a[7]");
	public static By mnuMyAccount = By.linkText("My Account");
	
	//Footer items
	public static By imgFooterLogo = By.xpath("//img[@alt='Powered by Mantis Bugtracker']");
	public static By colorSttNew = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[1]");
	public static By colorSttFeedback = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[2]	");
	public static By colorSttAcknowledged = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[3]");
	public static By colorSttConfirmed = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[4]");
	public static By colorSttAssigned = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[5]");
	public static By colorSttResolved = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[6]");
	public static By colorSttClosed = By.xpath("html/body/div[3]/table[2]/tbody/tr/td[7]");
}
